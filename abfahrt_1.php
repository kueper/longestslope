 <?php


    class map {

        private $filename;
        private $dh;
        private $mapRows;
        private $mapCols;
        private $map;
        private $erinnermich;
        private $here;
        private $row;
        private $column;
        private $tempPath;
        private $tempMoves;
        private $finalPath;
        private $countFinalPath;
        private $tempMap;
        private $finalSteigung;

        function map($fileName) {
            $this->filename = $fileName;
        }

        function getFilename() {
            return $this->filename;
        }

        public function getMap() {
            return $this->map;
        }

        function readFile() {
            $this->dh = fopen($this->filename, "r"); 
            $firstrow = explode(" ",$this->nextLine());
            $this->mapRows = $firstrow[0];
            $this->mapCols = $firstrow[1];
        }


        function nextLine() {
            if(!feof($this->dh))
                return fgets($this->dh);
        }

        function populateMap() {
            $r = 0;
            while(!feof($this->dh)) {
                $currentrow = explode(" ", $this->nextLine());
                for($c = 0; $c < count($currentrow); $c++) {
                    $this->map[$r][$c] = $currentrow[$c];
                    // print_r($this->map);
                    // $this->insertIntoMySQL("temppath",$this->map[$r][$c],$r,$c);
                }
                $r++;

            }

        }
        function MySQLConnection(){
            $user='root';
            $pass='';
            $db='zwischenablage';
            $conn= new mysqli('localhost', $user, $pass, $db) or die("Unable to connect to Database");
            // Check connection
            return $conn;  
        }
        function deleteOldValues($name){
            $sql = "TRUNCATE TABLE $name";
            $conn = $this->MySQLConnection();
            if (mysqli_query($conn, $sql)) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . mysqli_error($conn);
              }
            mysqli_close($conn);

        }
        function insertTempNull($name,$i,$j){
                    
            $sql = "UPDATE  $name SET temppath=0 WHERE i=`$i` AND j=`$j`";
            $conn = $this->MySQLConnection();

            if (mysqli_query($conn, $sql)) {
                echo "Null record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            }
        }

        function insertIntoMySQL($name, $array,$i,$j){ 
            // print_r($array);
            
            $sql = "INSERT INTO $name(temppath, i , j) VALUES ('$array','$i','$j') ";
            $conn = $this->MySQLConnection();

            if (mysqli_query($conn, $sql)) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . mysqli_error($conn);
              }

            mysqli_close($conn);
        }
         
        function readOutOfMySQL($name,$i,$j){
        // Read record
            $sql = "SELECT `$name` FROM $name WHERE i=`$i` AND j=`$j`";
            $conn = $this->MySQLConnection();
            $value=mysqli_query($conn, $sql);
            return $value;
            mysqli_close($conn);
        }

        function checkLengthTemp(){
            // echo count($this->tempPath), " ";
            if (count($this->tempPath) > $this->countFinalPath){
                $this->finalPath = $this->tempPath;  
                $this->countFinalPath = count($this->tempPath);
                $this->finalSteigung = (int)str_replace(' ', '', $this->finalPath[0])-(int)str_replace(' ', '', end($this->finalPath));
                echo " final: ";      
                print_r($this->finalPath);
                echo "\n\n";
                echo "Länge: ";
                echo $this->countFinalPath,"";
                echo "\n\n";
                echo"Steigung: ";
                echo $this->finalSteigung * -1,"";
            }
            // wenn zwei abfahrten gleich lang sind zählt die größere Steigung:
            if(count($this->tempPath) == $this->countFinalPath){
                $tempSteigung = (int)str_replace(' ', '', $this->tempPath[0])-(int)str_replace(' ', '',end($this->tempPath));
                $this->finalSteigung = (int)str_replace(' ', '', $this->finalPath[0])-(int)str_replace(' ', '', end($this->finalPath));

                if ($tempSteigung>$this->finalSteigung){            
                    $this->finalPath = $this->tempPath;  
                    $this->countFinalPath = count($this->tempPath);
                }
            }

        }
        function goToSmallerNo($index){
            $this->tempMoves[]=$index;
            if($index =="Up"){$this->column--;}
            elseif($index =="Down"){$this->column++;}
            elseif($index =="Left"){$this->row--;}
            elseif($index =="Right"){$this->row++;} 
        }

        function goBackToBiggerNo($index){ 
            if($index =="Up"){$this->column++;}
            elseif($index =="Down"){$this->column--;}
            elseif($index =="Left"){$this->row++;}
            elseif($index =="Right"){$this->row--;} 
        }

        function goOneBack(){
            $this-> checkLengthTemp();
            $errors = array_filter($this->tempMoves);        // wenn es überhaupt nicht existiert-> wenn nicht, sind keine kleinerer nachbarn da
            if (!empty($errors)) {

                $lastStep=array_pop($this->tempMoves);
                array_pop($this->tempPath);             // array pop entfernt das letzte element eines arrays -> bei Ursprung und abgecheckten nachbarn ist array leer
                array_pop($this->tempPath);             //zweimal, da nachher eh wieder ist state reingeschrieben wird
                $this->goBackToBiggerNo($lastStep);
                $this->checkDirections();
            } 
            $this->tempPath=array();
            $this->tempMoves=array();
            $this->erinnermich=0;
        }

        function checkDirections(){
            $j = $this->column;
            $i = $this->row;

            if($j< $this->mapCols && $i< $this->mapRows && $j >= 0 && $i >= 0){         // sicherung, dass durch das draufzählen die Dimensionen eingehalten werden
                if($this->erinnermich!=0){
                                                                //   // damit er nur einmal die ganze kiste durchzieht       
                    $this->here = $this->tempMap [$this->column][$this->row];
                    // $this->here = $this->readOutOfMySQL("temppath",$this->column,$this->row);
                    // print_r($this->tempMap[10]); echo" ";
                    $map= $this->tempMap;
                    $i=(int)($i);
                    $j=(int)($j);
                    // if($i+1 < ($this->mapRows)){$this->rechts = $this->readOutOfMySQL("temppath",$j,$i+1);} else{$this->rechts = 0;}
                    // if($i-1 !=-1){$this->links = $this->readOutOfMySQL("temppath",$j,$i-1);} else{$this->links = 0;} 
                    // if($j+1 < ($this->mapCols)){$this->unten = $this->readOutOfMySQL("temppath",$j+1,$i);} else{$this->unten = 0;}
                    // if($j-1 !=-1 ){$this->oben = $this->readOutOfMySQL("temppath",$j-1,$i);} else{$this->oben = 0;}
                    if($i+1 < $this->mapRows){$this->rechts = $map[$j][$i+1];} else{$this->rechts = 0;}
                    if($i-1 !=-1){$this->links = $map[$j][$i-1];} else{$this->links = 0;} 
                    if($j+1 < $this->mapCols){$this->unten = $map[$j+1][$i];} else{$this->unten = 0;}
                    if($j-1 !=-1 ){$this->oben = $map[$j-1][$i];} else{$this->oben = 0;}
                    // if($this->j > $this->column or  $this->i > $this->row){echo "heureka"; $this->isSecondTry;}
                    
                    $dir_arr = array( 
                        "Here"=>$this->here,
                        "Up"=>$this->oben,  
                        "Down"=>$this->unten,  
                        "Left"=>$this->links,  
                        "Right"=>$this->rechts
                    );
                    
                    $this->tempPath[]=$this->here;
                    $count=0;
                    
                    foreach($dir_arr as $index => $value) { 
                        if ($value<$this->here && $value !=0){
                            $this->goToSmallerNo($index);
                            $this->here=$value;
                            $count++;
                            $this->checkDirections();
                        }
                    }
                    // if ($count==0){ $this->insertTempNull("temppath",$this->column,$this->row); $this-> goOneBack();}  //tempmatrix = an der stelle= 0 -> verhindert endlosloop
                    if ($count==0){($this->tempMap[$this->column][$this->row]=0); $this-> goOneBack();} 
                }        
            }
        }

        
       function printThis($r) {
           echo "Zeile " . $r . "<br>";
       }     
        
        function iterateThroughMatrix(){

            //echo $this->getMapRows();

            for ($r = 0; $r < $this->mapRows; $r++) {

                $this->printThis($r);

                /*
                for ($c = 0; $c < $this->mapCols; $c++){
                    
                    $this->here = $this->map[$r][$c];
                    $this->row = $r;
                    $this->column = $c;
                    $this->erinnermich = 1;
                    $this->tempMap = $this->map;
                    
                    // $this->tempMap = $this->readOutOfMySQL("temppath");
                    if ($this->here >= $this->countFinalPath)
                    {
                       // $this->checkDirections();
                    }
                }
                */
            }
        }

    
            function justdo() {
                $time = microtime();
                $time = explode(' ', $time);
                $time = $time[1] + $time[0];
                $start = $time;

                // $this->deleteOldValues("temppath");
                $this->readFile();
                $this->populateMap();
                $this->iterarteThroughMatrice();
                $time = microtime();

                echo " final: ";      
                print_r($this->finalPath);
                echo "\n\n";
                echo "Länge: ";
                echo $this->countFinalPath,"";
                echo "\n\n";
                echo"Steigung: ";
                echo $this->finalSteigung * -1,"";

                $time = explode(' ', $time);
                $time = $time[1] + $time[0];
                $finish = $time;
                $total_time = round(($finish - $start), 4);
                echo 'Page generated in '.$total_time.' seconds.';
            }
    }
    // $myMap = new map('hoehen.txt');
    // $myMap = new map('map.txt');
    $myMap = new map('hunderter_matrix.txt');
    //$myMap->justdo();
    $myMap->readFile();
    $myMap->populateMap();
    //echo $myMap->getMapRows();
    $myMap->iterateThroughMatrix();
?>