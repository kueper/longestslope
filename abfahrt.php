
<?php





class map {

    private $filename;
    private $dh;
    private $mapRows;
    private $mapCols;
    private $map;
    private $tempGraph;
    private $tempPathCounter;
    private $finalPath;
    private $finalPathCounter;
    private $ist;
    private $rechts;
    private $links;
    private $unten;
    private $oben;
    private $k;
    private $l;
    private $i;
    private $j;
    private $up;
    private $down;
    private $left;
    private $right;
    private $lastStep;
    private $count;

    function map($fileName) {
        $this->filename = $fileName;
    }

    function getFilename() {
        return $this->filename;
    }

    public function getMap() {
        return $this->map;
    }

    function readFile() {
        $this->dh = fopen($this->filename, "r"); 
        $firstrow = explode(" ",$this->nextLine());
        $this->mapRows = $firstrow[0];
        $this->mapCols = $firstrow[1];
    }

    function nextLine() {
        if(!feof($this->dh))
            return fgets($this->dh);
    }

    function populateMap() {
        $r = 0;
        while(!feof($this->dh)) {
            $currentrow = explode(" ", $this->nextLine());
            for($c = 0; $c < count($currentrow); $c++) {
                $this->map[$r][$c] = $currentrow[$c];
            }
            $r++;

        }

    }
 
    function createTempGraph($index,$tempValue,$tempPathCounter){  
        $this->tempPath[] = ["Step"=>$index,"value"=>$tempValue,"l"=>$this->l,"k"=>$this->k];
        // $this->tempPath[] = "-";
        $this->tempPathCounter += $tempPathCounter;
        echo "check temp";
        print_r($this->tempPath);
    }
    
    function checkDirections($map){
  
        $j=$this->l;
        $i=$this->k;
        if($i+1 < ($this->mapRows)){$this->rechts = $map[$j][$i+1];} else{$this->rechts = 0;}
        if($i-1 !=-1){$this->links = $map[$j][$i-1];} else{$this->links = 0;} 
        if($j+1 < ($this->mapCols)){$this->unten = $map[$j+1][$i];} else{$this->unten = 0;}
        if($j-1 !=-1){$this->oben = $map[$j-1][$i];} else{$this->oben = 0;}

        $dir_arr = array( 
            "Up"=>$this->oben,  
            "Down"=>$this->unten,  
            "Left"=>$this->links,  
            "Right"=>$this->rechts
        ); 
        $this->count=0;
        foreach($dir_arr as $index => $value) { 
            
            // echo "sides[" . $index . "] => " . $value . " \n"; 
            if ($index !=$this->lastStep){$this->recursionPathFinder($index,$value,$map);}
            
           
            // $this->checkLengthTemp();
            // $this->createTempGraph($this->ist,1);$this->checkLengthTemp(); 
        } 
        
    }
  
    function setBackwardsReminder($index){
        // if($index=="Up"){$this->right=false; $this->left=false; $this->up=false; $this->down=true; $this->l--;}
        // elseif($index=="Down"){$this->right=false; $this->left=false; $this->up=true; $this->down=false; $this->l++;}
        // elseif($index=="Left"){$this->right=false; $this->left=true; $this->up=false; $this->down=false; $this->k--;}
        // elseif($index=="Right"){$this->right=true; $this->left;false; $this->up=false; $this->down=false; $this->k++;}
        if($index=="Up"){$this->l--;}
        elseif($index=="Down"){$this->l++;}
        elseif($index=="Left"){$this->k--;}
        elseif($index=="Right"){$this->k++;} 
    }
    // // function handleBackward($index){
    // //     if($index=="Up" && $this->up=true){echo $index;return true;}
    // //     elseif($index=="Down" && $this->down=true){echo $index;return true;}
    // //     elseif($index=="Left" && $this->left=true){echo $index;return true;}
    // //     elseif($index=="Right" && $this->right=true){echo $index;return true;}
    // //     else {return false;}
    // // }
    function backStep(){
            $this->lastStep=$this->tempPath[0];
            $this->k= ($this->tempPath[3]);
            $this->l= ($this->tempPath[2]);
            $this->checkDirections($this->map);
    }
    function checkLengthTemp(){
        if ($this->finalPathCounter < $this->tempPathCounter){$this->finalPath = $this->tempPath; 
            $this->finalPathCounter = $this->tempPathCounter;
            $this->tempPathCounter=0;
            $this->tempPath=array();
            
            // print_r($this->finalPath);
            // echo $this->finalPathCounter;
        }
        else{
            $this->tempPathCounter=0;
            $this->tempPath=array();
        }
    }
    function recursionPathFinder($index,$value,$map){
        // $pathFinder=true;
        // while ($pathFinder==true){
            // echo "ist";print_r($this->ist);
            
            if($value < $this->ist && $value!=0)      {$this->createTempGraph($index,$this->ist,1);  $this->setBackwardsReminder($index);     $this->ist= $value; $this-> checkDirections($map);$this->count=0;}
            
            // if($this->handleBackward($index)==false){echo "heureka";echo $index;}
            // if($this->rechts < $this->ist && $this->rechts!=0 && $left==false)      {$this->createTempGraph($this->ist,1);  echo "rechts ";print_r($this->rechts);  $k++;             $right=true;$left==false;$unten==false;$oben==false;    $this->ist= $this->rechts;$this-> checkDirections($map, $l, $k);}
            // elseif($this->links < $this->ist && $this->links!=0 && $right==false)   {$this->createTempGraph($this->ist,1);  echo "links ";print_r($this->links);    $k--;             $right=false; $left==true;$unten==false;$oben==false;   $this->ist= $this->links;$this-> checkDirections($map, $l, $k);}
            // elseif($this->unten < $this->ist && $this->unten!=0 && $oben==false)    {$this->createTempGraph($this->ist,1);  echo "unten ";print_r($this->unten);    $l++;             $right=false; $left==false;$unten==true;$oben==false;   $this->ist= $this->unten;$this-> checkDirections($map, $l, $k);}
            // elseif($this->oben < $this->ist && $this->oben!=0 && $unten==false)     {$this->createTempGraph($this->ist,1);  echo "oben ";print_r($this->oben);      $l--;             $right=false;$left==false;$unten==false; $oben==true;   $this->ist= $this->unten;$this-> checkDirections($map, $k, $l);}
            
            else{  $this->count++;} 
            // }
            if($this->count==4){$this-> checkLengthTemp();$this-> backStep();$this->l=0;$this->k=0;}
    }

    function checkNeighborhood($map){
        
    
        for ($this->j=0; $this->j < ($this->mapCols); $this->j++){
            for ($this->i=0; $this->i < ($this->mapRows); $this->i++){
                $this->ist = $map[$this->j][$this->i];
                echo "ist ";print_r($this->ist);  
                if($this->ist < $this->finalPathCounter){$this->i++;}
                else{
                $this->k=$this->i;
                $this->l=$this->j;
                $this-> checkDirections($map);
                }
            }
        }
    }

    function justdo() {
        $this->readFile();
        $this->populateMap();
        $this->checkNeighborhood($this->getMap());
    }
}



$myMap = new map('hoehen.txt');
$myMap->justdo();


?>
